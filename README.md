NCore torrent visited plugin for Chrome

store ncore visited torrent id intervals in the Chrome sync storage.

The visited torrents highlighted with green color, the not visited ones with red. Only in standard listing, not in search (at the moment)