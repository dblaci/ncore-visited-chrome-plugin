//a chrome sync mérete korlátozott 100kbyte. Ezért nem tudjuk az összes tárolt torrentet tárolni egyenként.
//helyette tól-ig tartományokat tárolunk, ami a folyamatos nézésnél eléggé hatékony lesz a tárolás szempontjából
//ideális esetben 1-12312312 kb. és kész.

let g_visited = []
const INTERVAL_HOLE_ALLOWED = 1 // a torrentek között van törölt. A sima listázás emiatt a tól-ig tartományokban hiányos, és ez növeli a sync felhasznált méretét.
				   //ezért ez a tolerancia 0 helyett lehet nagyobb, ennyi kimaradást még összefüggőnek vesz látottságnál
const INTERVAL_MAX_LENGTH = 5000 // too long intervals will slow down the plugin. It is unnecessary to store all visited to get the visited status.
const INTERVAL_MAX_COUNT = 50
let authToken = 'default'

if ($('#mire').val() !== '') {
	// kerseésnél nem nézzük a torrentek látogatottságát
} else {
	chrome.storage.local.get(['authToken'], function(result) {
		if (result.authToken) {
			authToken = result.authToken
		}
		loadIntervals()
	})

		//chrome.storage.local.get(null, function(data) {

	function loadIntervals() {
		fetch('https://dblaci.hu/ncore', {
			method: 'GET',
			headers: {
				'Authorization': 'Bearer ' + authToken
			}
		})
			.then(response => response.json())
			.then(data => {
				console.log('Data retrieved:', data)
				if ('intervals' in data) {
					$(data.intervals).each(function() {
						for (var i = parseInt(this.split('-')[0]); i <= parseInt(this.split('-')[1]); i++) {
							if (!isVisitedByTID(i)) {
								g_visited.push(i)
							}
						}
					})
				}

				console.log('Visited', g_visited) // DBG

				showTorrentStatusAndSave()
			})

	}

	function showTorrentStatusAndSave() {
		$('div.box_torrent').each(function() {
			var idt = $(this).find('a:eq(1)').attr('onclick').match(/torrent\(([0-9]+)\)/)
			$(this).append("<div style=\"clear:both;\"/>")
			const torrent_id = parseInt(idt[1])
			//               console.log(torrent_id) // DBG
			let szin
			if (isVisitedByTID(torrent_id)) {
				szin = "#0f0"
			} else {
				szin = "#f00"
				g_visited.push(torrent_id)
			}
			$(this).css('border', '1px solid ' + szin) // seen or not seen

		})

		saveIntervals()
	}

	function saveIntervals() {
		//chrome.storage.local.set(setList, function(error) {console.log(error)})
		let intervals = []

		let intervalFrom = -100
		let intervalTo = -100

		$(g_visited.sort()).each(function() {
			const id = parseInt(this)
			//console.log(intervalTo +' vs ' + this)
			// 1,2,3,5,6,7,8
			if (intervalTo + 1 + INTERVAL_HOLE_ALLOWED >= id) {
				intervalTo = id
			} else {
				if (intervalTo !== -100) {
					if (intervalFrom < intervalTo - INTERVAL_MAX_LENGTH) {
						// nem lehet akármekkora, levágjuk az elejét.
						intervalFrom = intervalTo - INTERVAL_MAX_LENGTH
					}
					intervals.push(intervalFrom + '-' + intervalTo)
				}
				// új intervallum
				intervalFrom = intervalTo = id
			}
		})
		// utolsó kintervallum most lesz pusholva
		if (intervalTo !== -100) {
			if (intervalFrom < intervalTo - INTERVAL_MAX_LENGTH) {
				intervalFrom = intervalTo - INTERVAL_MAX_LENGTH
			}
			intervals.push(intervalFrom + '-' + intervalTo)
		}

		console.log('save:', intervals) // DBG

		if (intervals.length > INTERVAL_MAX_COUNT) {
			intervals = intervals.splice(INTERVAL_MAX_COUNT)
		}

		fetch('https://dblaci.hu/ncore', {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': 'Bearer ' + authToken // Make sure to handle your auth token securely
			},
			body: JSON.stringify({'intervals': intervals})
		})
			.catch(error => console.error('Error saving data:', error))
	}

	function isVisitedByTID(tid) {
		if ($.inArray(tid, g_visited) !== -1) {
			return true
		}
		return false
	}
}
