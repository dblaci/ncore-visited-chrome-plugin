input = document.getElementById('authTokenInput')
chrome.storage.local.get(['authToken'], function(data) {
  console.log('hello', data.authToken)
  input.value = data.authToken
})
document.getElementById('saveButton').addEventListener('click', function() {
  let authToken = input.value;
  chrome.storage.local.set({ authToken: authToken }, function() {
    console.log('Auth Token is saved');
  });
});
